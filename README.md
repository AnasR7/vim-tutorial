# Basic vim keybinding


## Key binding with :

* `:e` = New file
* `:o` = open file
* `:w` = save file
* `:w <name_file>` = save file & named file
* `:q` = quit
* `:wq` = save & quit
* `:%s` = search
* `:terminal` = open terminal inside vim
* `:w! sudo tee %` = save with open root
* `:w!` = save with root
* `:vsp` = vertical split
* `:sp` = split
## key binding

* i = Insert Mode
* v = Visual Mode / block line
* V = Visual Mode All line
* Esc = Normal Mode
* d = delete line
* y = Yank
* p = Paste
* u = undo
* o = open newline
* O = Open upper newline
* h = Left
* j = Down
* k = Up
* l = Right

## Vim Installation

### Ubuntu
`sudo apt remove -y vim-tiny`
`sudo apt update`

if you want vim :
```
sudo apt install -y vim
```
if you want gvim :
```
sudo apt-get install vim-gnome
```

### Fedora
`yum install vim-X11`

### ArchLinux
if you want vim :
```

sudo pacman -S vim
```
if you want gvim
```
sudo pacman -S gvim
```
